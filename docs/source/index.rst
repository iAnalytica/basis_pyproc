$basis
$basis_v1

Contents:

.. toctree::
   :maxdepth: 2

   modules

.. only:: html

   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
